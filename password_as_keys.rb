require '_'
require 'openssl'
require 'digest'
require 'pry'

c = "c92b7734070205bdf6c0087a751466ec13ae15e6f1bcdd3f3a535ec0f4bbae66".unhex

File.open('words').each do |line|
	aes = OpenSSL::Cipher.new('AES-128-ECB')
	aes.decrypt
	key = Digest::MD5.hexdigest(line.chomp).unhex
	aes.key = key
	aes.padding = 0
	p = aes.update(c) + aes.final

	if p.start_with? "crypto"
		puts p
		binding.pry
		break
	end
end


