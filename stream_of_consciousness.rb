require 'net/http'
require 'json'
require '_'

$url = "https://aes.cryptohack.org/stream_consciousness"
def encrypt
	json = Net::HTTP.get(URI("#{$url}/encrypt/"))
	JSON.parse(json)["ciphertext"]
end

@log = "stream_of_consciousness.txt"

# sampling
def sampling
	list = []
	100.times do
		c = encrypt
		list << c if list.index(c).nil?
	end

	list.sort!
	File.write(@log, list.join("\n"))
end

@list = File.read(@log).split("\n")

def try(key)
	@list.map do |c|
		cc = c.unhex.xor(key + "\0" * 100)
		[cc[0...key.length], cc[key.length..-1]]
	end
end

flag = "crypto{"

#try1 = @list.map do |c|
#	key = c.unhex.xor(flag)
#	try(key[0...flag.length]).map{|p| p[0...flag.length]}
#end

# try1[0]
# da643612901586e2e84ae7ad2919d67ed35276e599a126a55306807ff5ad17b3

key = @list[0].unhex.xor(flag)[0...flag.length]

key += "\xfd".xor('t')
key += "\xfb".xor(' ')
key += "_".xor('l')
key += "\xbe".xor('l')
key += "\xf87".xor('bl')
key += "\n".xor(' ')
key += "\xc2".xor(' ')
key += "|\xFEEe".xor('ore ')
key += "\xf8\xc5\xfc\x1e\xb4".xor("hing ")
key += "\b>\xc6".xor("ng ")
key += "%\xa5\xb9".xor("ng ")
key += "[".xor(' ')
key += "\xee".xor(' ')

puts try(key)[0][0]

require 'pry'
binding.pry


