require 'net/http'
require 'json'
require '_'

@url = "https://aes.cryptohack.org/flipping_cookie"

def check_admin(cookie, iv)
	json = Net::HTTP.get(URI("#{@url}/check_admin/#{cookie}/#{iv}/"))
	JSON.parse(json)
end

def get_cookie()
	json = Net::HTTP.get(URI("#{@url}/get_cookie/"))
	JSON.parse(json)['cookie']
end

bsize = 16
c = get_cookie().unhex
iv = c[0...bsize]
cookie = c[bsize..-1]

f = "admin=False;expiry="[0...16]
t = "admin=True;eexpiry="[0...16]

iv = iv.xor(f).xor(t)
ck check_admin(cookie.hexstr, iv.hexstr)

p ck["flag"]

