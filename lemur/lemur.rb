require 'chunky_png'

p1 = ChunkyPNG::Image.from_file('lemur.png')
p2 = ChunkyPNG::Image.from_file('flag.png')

abort "w h" unless p1.width == p2.width and p1.height == p2.height

width = p1.width
height = p1.height
p3 = ChunkyPNG::Image.new(width, height, ChunkyPNG::Color::TRANSPARENT)

width.times do |w|
	height.times do |h|
		x = p1[w, h] ^ p2[w, h]
		p3[w, h] = x | 0xff
	end
end

p3.save('out.png')

