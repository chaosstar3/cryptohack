require 'net/http'
require 'json'
require '_'

@url = "https://aes.cryptohack.org/bean_counter"

def encrypt()
#	json = Net::HTTP.get(URI("#{@url}/encrypt/"))
#	JSON.parse(json)["encrypted"]
	File.read("bean_counter.enc")
end

png_header = "89504e470d0a1a0a"
png_header += "0000000d" + "IHDR".hexstr

e = encrypt

key = e[0...32].unhex.xor(png_header.unhex)
d = e.unhex.xor(key)
File.write("bean_counter.png", d)

# let's data carving
require 'pry'

0.step(e.length, 32) do |i|
	x = e[i...(i+32)].unhex.xor(key)
	p x
	#binding.pry
end



