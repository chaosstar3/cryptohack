require 'net/http'
require 'json'
require '_'

@url = "https://aes.cryptohack.org/ctrime"

def encrypt(ptext)
	json = Net::HTTP.get(URI("#{@url}/encrypt/#{ptext}/"))
	JSON.parse(json)["ciphertext"].length
end

key = "crypto{CRIME_571ll_p4yS"

@set = ('a'..'z').to_a + ('0'..'9').to_a + ('A'..'Z').to_a + "_ {}".split('') + "()[]".split('') + "!?,.@\#$%^&|*+-=/<>;:'\"`~\\".split('')

def guess(prefix)
	min = 9999

	h = {}
	@set.each do |c|
		chal = (prefix + c) * 5
		len = encrypt(chal.hexstr)
		h[c] = len
		puts "#{c} #{len}"

		if len < min
			if min == 9999
				min = len
			else
				min = len
				return c, h
			end
		end
	end
	return nil, h
end

require 'pry'

loop do
	c, h = guess(key)
	if c.nil?
		binding.pry
	else
		key += c
		puts key
		break if c == '}' # end
	end
end

puts key

binding.pry


