require 'net/http'
require 'json'
require '_'

@url = "https://aes.cryptohack.org/lazy_cbc"

def encrypt(ptext)
	json = Net::HTTP.get(URI("#{@url}/encrypt/#{ptext}/"))
	JSON.parse(json)["ciphertext"]
end

def get_flag(key)
	json = Net::HTTP.get(URI("#{@url}/get_flag/#{key}/"))
	JSON.parse(json)
end

def receive(ctext)
	json = Net::HTTP.get(URI("#{@url}/receive/#{ctext}/"))
	JSON.parse(json)
end

require 'pry'

c = encrypt("00" * 32)
c1 = c[0...32]
c2 = c[32...64]
r = receive(c2)

if r["error"]
	iv = r["error"].split(' ')[2]
	key = iv.unhex.xor(c1.unhex)

	flag = get_flag(key.hexstr)

	if flag["plaintext"]
		p flag["plaintext"].unhex
	end

else
	puts "try another"
end
