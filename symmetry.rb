require 'net/http'
require 'json'
require '_'

@url = "https://aes.cryptohack.org/symmetry"

def encrypt(ptext, iv)
	json = Net::HTTP.get(URI("#{@url}/encrypt/#{ptext}/#{iv}/"))
	JSON.parse(json)["ciphertext"]
end

def encrypt_flag()
	json = Net::HTTP.get(URI("#{@url}/encrypt_flag/"))
	c = JSON.parse(json)["ciphertext"]
	return c[0...32], c[32..-1]
end

iv, ctext = encrypt_flag()
ptext = ""

0.step(ctext.length , 16*2) do |i|
	output = encrypt("00"*16, iv)
	pblock = output.unhex.xor(ctext[i...(i+32)].unhex)
	ptext += pblock
	iv = output
end

puts ptext

