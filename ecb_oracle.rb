require 'net/http'
require 'json'
require '_'

def encrypt(ptext)
	url = "https://aes.cryptohack.org/ecb_oracle"
	json = Net::HTTP.get(URI("#{url}/encrypt/#{ptext}/"))
	JSON.parse(json)["ciphertext"]
end

# AES-128-ECB
blocksize = 16
keylen = 25
key = "crypto{p3n6u1n5_h473_3cb}"

if keylen.nil? or  keylen == 0
	# Get key length
	len = encrypt("00").length / 2
	(2..16).each do |i|
		test_len = encrypt("00" * i).length / 2
		if test_len > len
			keylen = (test_len - blocksize) - i
			break
		end
	end
end

set = ('a'..'z').to_a + ('0'..'9').to_a + "_ {}()[]".split('') + ('A'..'Z').to_a + "!?,.@\#$%^&|*+-=/<>;:'\"`~\\".split('')

(keylen - key.length).downto(1) do |i|
	shift = (-i % blocksize) + 1
	block_inspect = - (((keylen - i + 1) / blocksize) * blocksize * 2)
	block = ((block_inspect - blocksize * 2) .. (block_inspect - 1))

	target = encrypt("00" * shift)[block]

	c = set.find do |c|
		chal = "#{c}#{key}".pad(blocksize)
		resp = encrypt(chal.hexstr)[0...(blocksize * 2)]
		(target == resp)
	end

	if c
		key = c + key
		puts key
	else
		binding.pry
	end
end

p key
