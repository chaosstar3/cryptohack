require 'net/http'
require 'json'
require '_'

def encrypt_flag()
	url = "https://aes.cryptohack.org/ecbcbcwtf"
	json = Net::HTTP.get(URI("#{url}/encrypt_flag/"))
	JSON.parse(json)["ciphertext"]
end

def decrypt(ctext)
	url = "https://aes.cryptohack.org/ecbcbcwtf"
	json = Net::HTTP.get(URI("#{url}/decrypt/#{ctext}/"))
	JSON.parse(json)["plaintext"]
end

bsize = 16*2
ef = encrypt_flag()
iv = ef[0...bsize]
ctext = ef[bsize..-1]

flag = ""
x = iv
0.step(ctext.length - 1, bsize).each do |i|
	cblock = ctext[i ... (i+bsize)]
	flag += decrypt(cblock).unhex.xor(x.unhex)
	x = cblock
end

p flag

