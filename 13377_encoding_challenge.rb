require 'socket'
require 'json'
require 'base64'
require '_'

s = TCPSocket.new('socket.cryptohack.org', 13377)

100.times do |i|
	line = s.readline
	puts "< #{line}"
	json = JSON.load(line)
	e = json['encoded']

	case json['type']
	when 'base64'
		d = Base64.decode64(e)
	when 'hex'
		d = e.unhex
	when 'rot13'
		d = rot(e, 13)
	when 'bigint'
		d = e[2..-1].unhex
	when 'utf-8'
		d = e.map(&:chr).join
	else
		binding.pry
	end

	puts "> #{d}"
	s.puts({"decoded" => d}.to_json)
end

puts s.readline
s.close
