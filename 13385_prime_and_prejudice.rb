require 'socket'
require 'json'
require '_'
require 'pry'


p = (1<<576) * 0x0000000000000000000000bc508ae6dacc43b138c0e9f22d
p += (1<<384) * 0xfb99b146bedd0ac93f84e8cfe2780a881fdbad85918a6b75
p += (1<<192) * 0xbd3af841123bad7438fe08c5433ec8b5fa7b0a1b149876bf
p += 0x5af73cd9a608317066029e0cff4171ce336ff0b666344757


p1 = 142445387161415482404826365418175962266689133006163
p2 = 5840260873618034778597880982145214452934254453252643
p3 = 14386984103302963722887462907235772188935602433622363


binding.pry



b = (1 << 600) + 1
e = 1<<900

basis = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61]

def mr_test(p)
	basis = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61]
	!basis.find {|b| !miller_rabin(p, b)}
end

def find_mrprime(b)
	loop do
		break b if miller_rabin(b)
		b+= 2
	end
end

#p = (1<<600) + 187
#p = find_mrprime((1<<300) + 1)
#q = p

#loop do
#	break if mr_test(p * q)
#	q = find_mrprime(q+2)
#end

s = TCPSocket.new('socket.cryptohack.org', 13385)
puts s.readline
s.puts({'prime' => p, 'base' => 3}.to_json)

resp = s.readline
puts resp

s.close

binding.pry
