from Crypto.Cipher import AES
import hashlib

c = "c92b7734070205bdf6c0087a751466ec13ae15e6f1bcdd3f3a535ec0f4bbae66"

def decrypt(ciphertext, password_hash):
	ciphertext = bytes.fromhex(ciphertext)
	#key = bytes.fromhex(password_hash)
	key = password_hash

	cipher = AES.new(key, AES.MODE_ECB)
	try:
		decrypted = cipher.decrypt(ciphertext)
	except ValueError as e:
		return "error"

	return decrypted


with open('words') as f:
	for word in f.readlines():
		key = hashlib.md5(word.strip().encode()).digest()
		p = decrypt(c, key)

		if p.startswith(b'crypto'):
			print(c)
			print(word)
			print(key)
			print(p)
